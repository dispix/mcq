import styled from "styled-components"

export default styled.button`
  border-radius: 4px;
  padding: 4px;
  cursor: pointer;
  opacity: ${props => (props.disabled ? "0.7" : "1")};
`
