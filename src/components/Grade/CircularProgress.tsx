import React from "react"
import styled from "styled-components"

export interface IProps {
  size: number
  score: number
  max: number
  strokeWidth: number
  color: string
}

const CircleBackground = styled.circle`
  fill: none;
  stroke: #ddd;
`
const CircleProgress = styled.circle`
  fill: none;
  stroke-linecap: round;
  stroke-linejoin: round;
`
const Text = styled.text`
  font-size: 3em;
  font-weight: bold;
`

function CircularProgress({ size, strokeWidth, score, max, color }: IProps) {
  const percentage = Math.round((score / max) * 100)
  // SVG centers the stroke width on the radius, subtract out so circle fits in square
  const radius = (size - strokeWidth) / 2
  // Enclose cicle in a circumscribing square
  const viewBox = `0 0 ${size} ${size}`
  // Arc length at 100% coverage is the circle circumference
  const dashArray = radius * Math.PI * 2
  // Scale 100% coverage overlay with the actual percent
  const dashOffset = dashArray - (dashArray * percentage) / 100

  return (
    <svg width={size} height={size} viewBox={viewBox}>
      <CircleBackground
        cx={size / 2}
        cy={size / 2}
        r={radius}
        strokeWidth={`${strokeWidth}px`}
      />
      <CircleProgress
        cx={size / 2}
        cy={size / 2}
        r={radius}
        stroke={color}
        strokeWidth={`${strokeWidth}px`}
        // Start progress marker at 12 O'Clock
        transform={`rotate(-90 ${size / 2} ${size / 2})`}
        style={{
          strokeDasharray: dashArray,
          strokeDashoffset: dashOffset,
        }}
      />
      <Text
        x="50%"
        y="50%"
        dy=".3em"
        textAnchor="middle"
        fill={color}
        style={{ fontSize: `${3 * (size / 200) * 0.9}em` }}
      >
        {Math.round(score)}/{max}
      </Text>
    </svg>
  )
}

CircularProgress.defaultProps = {
  size: 200,
  strokeWidth: 10,
}

export default CircularProgress
