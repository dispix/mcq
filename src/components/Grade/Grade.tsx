import * as React from "react"
import { Spring } from "react-spring"

import CircularProgress from "./CircularProgress"

export interface IProps {
  score: number
  max: number
}

function Grade(props: IProps) {
  return (
    <Spring delay={300} from={{ score: 0 }} to={{ score: props.score }}>
      {({ score }) => (
        <CircularProgress
          size={100}
          color="#ab47bcff"
          score={score}
          max={props.max}
        />
      )}
    </Spring>
  )
}

export default Grade
