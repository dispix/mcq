import * as React from "react"
import { Trail, TrailProps } from "react-spring"
import styled from "styled-components"

interface IProps {
  children: TrailProps<any>["items"]
  keys: TrailProps<any>["keys"]
}

const Ul = styled.ul`
  list-style-type: none;
  width: 800px;
  display: flex;
  flex-wrap: wrap;
  flex-direction: row;
  justify-content: center;
`

function List({ keys, children }: IProps) {
  return (
    <Ul>
      <Trail
        items={children}
        keys={keys}
        from={{ transform: "translate3d(-40px,0,0)", opacity: 0 }}
        to={{ transform: "translate3d(0,0,0)", opacity: 1 }}
      >
        {child => style => <li style={style}>{child}</li>}
      </Trail>
    </Ul>
  )
}

export default React.memo(List)
