import * as React from "react"
import { Link } from "react-router-dom"
import styled from "styled-components"

interface IMcq {
  id: string
  title: string
  questions: any[]
  backgroundUri: string | null
}

interface IProps {
  mcq: IMcq
}

const Container = styled.div`
  position: relative;
  margin: 8px;
  border-radius: 4px;
  width: 300px;
  height: 200px;
  overflow: hidden;
  background-color: #eceff1ff;
  cursor: pointer;

  img {
    transition: transform ease-in 0.5s;
  }

  :hover {
    img {
      transform: scale(1.2);
    }
  }
`

const Illustration = styled.img`
  z-index: 0;
  position: absolute;
  border-radius: 4px;
  width: 100%;
  height: 100%;
`

const Placeholder = styled.div`
  z-index: 0;
  position: absolute;
  border-radius: 4px;
  width: 100%;
  height: 100%;
  background-color: #ab47bcff;
`

const Description = styled.p`
  z-index: 1;
  position: absolute;
  bottom: 16px;
  left: 16px;
  color: white;
  font-weight: bold;
`

function McqCard({ mcq }: IProps) {
  return (
    <Link key={mcq.id} to={`/mcqs/${mcq.id}`}>
      <Container>
        {mcq.backgroundUri ? (
          <Illustration src={mcq.backgroundUri} />
        ) : (
          <Placeholder />
        )}
        <Description>
          {mcq.title} ({mcq.questions.length} questions)
        </Description>
      </Container>
    </Link>
  )
}

export default McqCard
