import * as React from "react"
import { Link } from "react-router-dom"

import List from "../List/List"
import McqCard from "../McqCard"

interface IMcq {
  id: string
  title: string
  questions: any[]
  backgroundUri: string | null
}
interface IProps {
  mcqs: IMcq[]
}

function McqList(props: IProps) {
  return (
    <List keys={props.mcqs.map(mcq => mcq.id)}>
      {props.mcqs.map(mcq => (
        <McqCard key={mcq.id} mcq={mcq} />
      ))}
    </List>
  )
}

export default React.memo(McqList)
