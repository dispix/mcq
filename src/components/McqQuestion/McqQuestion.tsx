import * as React from "react"
import { Field, FieldProps } from "react-final-form"
import styled from "styled-components"

interface IProps extends FieldProps {
  title: string
  answers: string[]
}

const Container = styled.div`
  display: flex;
  flex-direction: column;
`

const Label = styled.label`
  padding: 2px;
  margin: 4px;
  transition: all ease-in 0.2s;

  :hover {
    background-color: rgba(0, 0, 0, 0.2);
  }
`

function McqQuestion({ title, answers, ...fieldProps }: IProps) {
  return (
    <Container>
      <label>
        <h3>{title}</h3>
      </label>
      {answers.map((answer, index) => (
        <Label key={index}>
          <Field
            {...fieldProps}
            component="input"
            type="checkbox"
            value={index}
          />{" "}
          {answer}
        </Label>
      ))}
    </Container>
  )
}

export default React.memo(McqQuestion)
