import * as React from "react"

import Grade, { IProps as GradeProps } from "../Grade"

export interface IProps extends GradeProps {
  name: string
}

function McqResult(props: IProps) {
  return <Grade score={props.score} max={props.max} />
}

export default React.memo(McqResult)
