import * as React from "react"
import { Form } from "react-final-form"

interface IProps {
  onSubmit: (data: object) => void
  children: Array<React.ReactElement<any>>
}

function Wizard({ onSubmit, children }: IProps) {
  const [page, setPage] = React.useState(0)
  const [currentValues, setValues] = React.useState({})
  const activePage = React.Children.toArray(children)[
    page
  ] as React.ReactElement<any>
  const isLastPage = page === React.Children.count(children) - 1

  const next = (values: object) => {
    setPage(Math.min(page + 1, children.length - 1))
    setValues(values)
  }
  const previous = () => {
    setPage(Math.max(page - 1, 0))
  }
  const validate = (values: object) =>
    activePage.props.validate ? activePage.props.validate(values) : {}
  const submit = (values: object) =>
    isLastPage ? onSubmit(values) : next(values)

  return (
    <Form initialValues={currentValues} validate={validate} onSubmit={submit}>
      {({ handleSubmit, submitting, values }) => (
        <form onSubmit={handleSubmit}>
          {activePage}
          <div className="buttons">
            {page > 0 && (
              <button type="button" onClick={previous}>
                « Previous
              </button>
            )}
            {!isLastPage && <button type="submit">Next »</button>}
            {isLastPage && (
              <button type="submit" disabled={submitting}>
                Submit
              </button>
            )}
          </div>
        </form>
      )}
    </Form>
  )
}

Wizard.Page = ({ children }: { children: React.ReactElement<any> }) => children

export default Wizard
