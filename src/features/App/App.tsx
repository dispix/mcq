import React from "react"
import { Route, Switch } from "react-router-dom"
import { ThemeProvider } from "styled-components"

import HomePage from "../../pages/HomePage"
import LoginPage from "../../pages/LoginPage"
import theme from "../../services/theme"
import AppBar from "../AppBar/AppBar"
import Authentication from "../Authentication"
import Router from "../Router"

import Container from "./Container"
import McqRoute from "./McqRoute"

function App() {
  return (
    <Authentication>
      <ThemeProvider theme={theme}>
        <Router>
          <React.Fragment>
            <AppBar />
            <Container>
              <Switch>
                <Route exact={true} path="/login" component={LoginPage} />
                <McqRoute exact={true} path="/mcqs/:id" />
                <Route path="" component={HomePage} />
              </Switch>
            </Container>
          </React.Fragment>
        </Router>
      </ThemeProvider>
    </Authentication>
  )
}

export default App
