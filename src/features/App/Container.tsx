import styled from "styled-components"

import { appBarHeight } from "../../services/theme"

const Container = styled.div`
  width: 100vw;
  height: calc(100vh - ${appBarHeight});
  margin-top: ${appBarHeight};
`

export default Container
