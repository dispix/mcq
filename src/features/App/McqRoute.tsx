import * as React from "react"
import { Redirect, Route, RouteProps } from "react-router"

import { authenticationContext } from "../Authentication"

import McqStudentPage from "../../pages/McqStudentPage"
import McqTeacherPage from "../../pages/McqTeacherPage"

function McqRoute(props: RouteProps) {
  const { currentUser } = React.useContext(authenticationContext)

  if (!currentUser) {
    return <Redirect to="/login" />
  }

  const component =
    currentUser.role === "STUDENT" ? McqStudentPage : McqTeacherPage

  return <Route {...props} component={component} />
}

export default McqRoute
