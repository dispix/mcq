import * as React from "react"
import { Link } from "react-router-dom"
import styled from "styled-components"

import Button from "../../components/Button"
import { appBarHeight } from "../../services/theme"
import { authenticationContext } from "../Authentication"

import appIcon from "./app-icon.svg"
import logoutIcon from "./logout-icon.svg"

const Header = styled.header`
  position: fixed;
  top: 0;
  width: 100vw;
  height: ${appBarHeight};
  box-shadow: -1px 2px 20px 1px rgba(0, 0, 0, 0.75);
`

const UserName = styled.p`
  margin: auto;
`

const Icon = styled.img`
  margin: 2px;
  height: calc(${appBarHeight} - 4px);
  cursor: pointer;
`

const Logout = styled(Icon).attrs({
  alt: "Logout",
  src: logoutIcon,
})`
  height: calc(${appBarHeight} - 8px);
  margin: 4px;
`

const RightSide = styled.div`
  float: right;
  display: flex;
`

function AppBar() {
  const { currentUser, logout } = React.useContext(authenticationContext)

  return (
    <Header>
      <Link to="/">
        <Icon src={appIcon} />
      </Link>
      {currentUser && (
        <RightSide>
          <UserName>{currentUser.name}</UserName>
          <Logout onClick={logout} />
        </RightSide>
      )}
    </Header>
  )
}

export default AppBar
