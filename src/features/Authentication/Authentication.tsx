import * as React from "react"

import { User } from "../../services/api"

interface IUser {
  name: string
  role: "TEACHER" | "STUDENT"
}
interface ICurrentUser extends IUser {
  id: string
}
interface IProps {
  children: React.ReactChild
}
interface IContext {
  /**
   * ID of the user currently logged in.
   */
  currentUser: null | ICurrentUser
  /**
   * Gets a user name and role and tries to authenticate.
   */
  login: (user: IUser) => void
  logout: () => void
}

const authenticationContext = React.createContext<IContext>({} as IContext)

function Authentication(props: IProps) {
  const [currentUser, setCurrentUser] = React.useState<IContext["currentUser"]>(
    null,
  )
  React.useEffect(() => {
    const user = localStorage.getItem("currentUser")
    if (user) {
      setCurrentUser(JSON.parse(user))
    }
  }, [])
  const login: IContext["login"] = user => {
    User.findId(user).then(id => {
      if (id) {
        setCurrentUser({ ...user, id })
        localStorage.setItem("currentUser", JSON.stringify({ ...user, id }))
      }
    })
  }
  const logout: IContext["logout"] = () => {
    setCurrentUser(null)
    localStorage.removeItem("currentUser")
  }

  return (
    <authenticationContext.Provider value={{ currentUser, login, logout }}>
      {props.children}
    </authenticationContext.Provider>
  )
}

export { authenticationContext }
export default Authentication
