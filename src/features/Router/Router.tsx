import React, { ReactChild } from "react"
import { BrowserRouter } from "react-router-dom"

function Router(props: { children: ReactChild }) {
  return <BrowserRouter>{props.children}</BrowserRouter>
}

export default Router
