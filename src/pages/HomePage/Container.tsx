import styled from "styled-components"

import { appBarHeight, studentColor, teacherColor } from "../../services/theme"

interface IProps {
  role: "STUDENT" | "TEACHER"
}

export default styled.div<IProps>`
  width: 100vw;
  height: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  background-color: ${props =>
    props.role === "STUDENT" ? studentColor(props) : teacherColor(props)};
`
