import * as React from "react"
import { unstable_createResource } from "react-cache"
import { Redirect } from "react-router"

import McqList from "../../components/McqList"
import { authenticationContext } from "../../features/Authentication"
import { Mcq } from "../../services/api"

import Container from "./Container"

const mcqsResource = unstable_createResource(Mcq.getAll)

function HomePage() {
  const { currentUser, logout } = React.useContext(authenticationContext)
  if (!currentUser) {
    return <Redirect to="/login" />
  }

  const mcqs = mcqsResource.read("")

  return (
    <Container role={currentUser.role}>
      <h2>Welcome back, {currentUser.name}!</h2>
      {!!mcqs.length && <McqList mcqs={mcqs} />}
    </Container>
  )
}

export default HomePage
