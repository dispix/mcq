import * as React from "react"

import HomePage from "./HomePage"

function SuspenseWrapper() {
  return (
    <React.Suspense fallback={null}>
      <HomePage />
    </React.Suspense>
  )
}

export default SuspenseWrapper
