import * as React from "react"
import styled from "styled-components"

interface IGradient {
  from: string
  to: string
}

interface IProps {
  children: React.ReactChild
  gradient: IGradient
  position: "LEFT" | "RIGHT"
}

const colorFrom = (props: IProps) => props.gradient.from
const colorTo = (props: IProps) => props.gradient.to
const position = (props: IProps) => (props.position === "LEFT" ? "100%" : "0%")

const Background = styled.div<IProps>`
  width: 50vw;
  height: 100vh;
  background: ${colorFrom};
  background: radial-gradient(
    circle at ${position},
    ${colorFrom} 0%,
    ${colorTo} 50%
  );
`

export default Background
