import * as React from "react"
import { Field, FieldProps } from "react-final-form"
import styled from "styled-components"

const FieldSet = styled.fieldset`
  border: none;
  display: flex;
  padding: 0;
`

const Input = styled.input`
  border: none;
  border-radius: 4px;
  padding: 4px;
  margin: 8px 0;
`

function LoginField(props: Partial<FieldProps>) {
  return (
    <FieldSet>
      <label htmlFor="login-name">Name</label>
      <Field
        {...props}
        id="login-name"
        name="name"
        type="text"
        placeholder="Enter your name"
      >
        {({ input }) => <Input {...input} />}
      </Field>
    </FieldSet>
  )
}

export default LoginField
