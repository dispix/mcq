import * as React from "react"
import { Field, Form } from "react-final-form"
import { Spring } from "react-spring"
import styled from "styled-components"

import LoginField from "./LoginField"
import Submit from "./Submit"

interface IFormData {
  name: string
}
interface IProps {
  onSubmit: (form: IFormData) => void
  for: string
}

const StyledForm = styled.form`
  height: 100%;
  width: fit-content;
  margin: 50% auto;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
`

const Title = styled.h2`
  align-self: left;
`

function LoginForm(props: IProps) {
  return (
    <Form
      // @ts-ignore
      onSubmit={props.onSubmit}
      render={({ handleSubmit }) => (
        <StyledForm onSubmit={handleSubmit}>
          <Spring
            delay={500}
            from={{ opacity: 0, transform: "translate(20px)" }}
            to={{ opacity: 1, transform: "translate(0px)" }}
          >
            {style => <Title style={style}>{props.for}</Title>}
          </Spring>
          <Spring
            delay={600}
            from={{ opacity: 0, transform: "translate(20px)" }}
            to={{ opacity: 1, transform: "translate(0px)" }}
          >
            {style => (
              <div style={style}>
                <LoginField />
                <Submit />
              </div>
            )}
          </Spring>
        </StyledForm>
      )}
    />
  )
}

export default LoginForm
