import * as React from "react"
import { Redirect } from "react-router-dom"

import { authenticationContext } from "../../features/Authentication"

import Background from "./Background"
import Container from "./Container"
import LoginForm from "./LoginForm"

function LoginPage() {
  const { login, currentUser } = React.useContext(authenticationContext)

  if (currentUser) {
    return <Redirect to="/" />
  }

  return (
    <Container>
      <Background gradient={{ from: "#FFBBBB", to: "#E08484" }} position="LEFT">
        <LoginForm
          for="Teacher"
          onSubmit={data => login({ ...data, role: "TEACHER" })}
        />
      </Background>
      <Background
        gradient={{ from: "#B2F2FC", to: "#23BFE0" }}
        position="RIGHT"
      >
        <LoginForm
          for="Student"
          onSubmit={data => login({ ...data, role: "STUDENT" })}
        />
      </Background>
    </Container>
  )
}

export default LoginPage
