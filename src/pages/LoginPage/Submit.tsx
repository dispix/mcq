import * as React from "react"
import { Spring } from "react-spring"
import styled from "styled-components"

const Button = styled.button`
  border: none;
  border-radius: 4px;
  padding: 8px;
  margin: 8px 0;
  color: rgb(255, 255, 255);
  background-color: rgba(0, 0, 0, 0.2);
  cursor: pointer;
`

function Submit() {
  const [isHovered, setHovered] = React.useState(false)
  const onMouseEnter = React.useCallback(setHovered.bind(null, true), [])
  const onMouseLeave = React.useCallback(setHovered.bind(null, false), [])
  const from = {
    scale: 1,
    translate: 0,
  }
  const to = {
    scale: isHovered ? 1.2 : 1,
    translate: isHovered ? 15 : 0,
  }

  return (
    <Spring from={from} to={to}>
      {({ scale, translate }) => (
        <Button
          style={{
            transform: `scale(${scale}) translate(${translate}%, ${translate}%)`,
          }}
          type="submit"
          onMouseEnter={onMouseEnter}
          onMouseLeave={onMouseLeave}
        >
          Login
        </Button>
      )}
    </Spring>
  )
}

export default Submit
