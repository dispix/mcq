import { FORM_ERROR } from "final-form"
import compose from "ramda/es/compose"
import keys from "ramda/es/keys"
import values from "ramda/es/values"
import * as React from "react"
import { Form } from "react-final-form"
import styled from "styled-components"

import Button from "../../components/Button"
import McqQuestion from "../../components/McqQuestion"
import { IMcq } from "../../services/api"

interface IProps {
  mcq: IMcq
  onSubmit: (data: number[][]) => void
}

const StyledForm = styled.form`
  width: 45vw;
  height: 90vh;
  overflow: scroll;
`

function McqForm(props: IProps) {
  const onSubmit = compose(
    props.onSubmit,
    values,
  )
  const validate = (data: object) =>
    keys(data).length !== props.mcq.questions.length ? FORM_ERROR : {}

  return (
    <Form
      onSubmit={onSubmit}
      validate={validate}
      render={({ handleSubmit, valid }) => (
        <React.Fragment>
          <StyledForm onSubmit={handleSubmit} id="mcq-form">
            {props.mcq.questions.map((question, index) => (
              <McqQuestion
                key={index}
                name={`question_${index}`}
                title={question.title}
                answers={question.availableAnswers}
              />
            ))}
          </StyledForm>
          <Button disabled={!valid} type="submit" form="mcq-form">
            Submit
          </Button>
        </React.Fragment>
      )}
    />
  )
}

export default McqForm
