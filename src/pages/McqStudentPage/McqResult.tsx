import * as React from "react"
import { Link } from "react-router-dom"
import styled from "styled-components"

import Button from "../../components/Button"
import McqResultComponent, {
  IProps as McqResultComponentProps,
} from "../../components/McqResult"
import { getGrade, IMcq, ISubmission, Mcq } from "../../services/api"

interface IProps {
  mcq: IMcq
  submission: ISubmission
  name: McqResultComponentProps["name"]
  onDelete: () => void
}

const Container = styled.div`
  height: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  padding-bottom: 50px;
`

const StyledButton = styled(Button)`
  margin-top: 16px;
`

function McqResult({ name, mcq, submission, onDelete }: IProps) {
  return (
    <Container>
      <h3>Thank you for submitting the MCQ, your score:</h3>
      <McqResultComponent
        name={name}
        score={getGrade(mcq, submission)}
        max={mcq.questions.length}
      />
      <StyledButton onClick={onDelete}>Retake MCQ</StyledButton>
      <Link to="/">
        <StyledButton>Back</StyledButton>
      </Link>
    </Container>
  )
}

export default McqResult
