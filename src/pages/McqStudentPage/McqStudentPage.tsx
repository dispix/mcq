import * as React from "react"
import { Redirect, RouteComponentProps } from "react-router"

import { authenticationContext } from "../../features/Authentication"
import { Mcq } from "../../services/api"

import McqForm from "./McqForm"
import mcqResource from "./mcqResource"
import McqResult from "./McqResult"
import useSubmission from "./useSubmission"

interface IRouteParams {
  id: string
}

export type Props = RouteComponentProps<IRouteParams>

function McqStudentPage(props: Props) {
  const { currentUser } = React.useContext(authenticationContext)
  const [submitted, setSubmitted] = React.useState(false)

  if (!currentUser) {
    return <Redirect to="/login" />
  }

  const mcq = mcqResource.read(props.match.params.id)

  if (!mcq) {
    return <Redirect to="/" />
  }

  const { submission, loading } = useSubmission(
    props.match.params.id,
    currentUser.id,
    submitted,
  )

  if (loading) {
    return null
  }

  const hasSubmission = <T extends any>(sub: T): sub is NonNullable<T> => !!sub
  const onSubmit = async (data: number[][]) => {
    setSubmitted(false)
    Mcq.submitAnswers(mcq.id, currentUser.id, data).then(() => {
      setSubmitted(true)
    })
  }
  const onDelete = () => {
    if (hasSubmission(submission)) {
      setSubmitted(false)
      Mcq.deleteSubmission(mcq.id, submission.id).then(() => {
        setSubmitted(true)
      })
    }
  }

  return hasSubmission(submission) ? (
    <McqResult
      mcq={mcq}
      submission={submission}
      name={currentUser.name}
      onDelete={onDelete}
    />
  ) : (
    <McqForm mcq={mcq} onSubmit={onSubmit} />
  )
}

export default McqStudentPage
