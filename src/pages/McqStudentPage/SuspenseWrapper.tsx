import * as React from "react"

import Container from "./Container"
import McqStudentPage, { Props } from "./McqStudentPage"

function SuspenseWrapper(props: Props) {
  return (
    <Container>
      <React.Suspense fallback={null}>
        <McqStudentPage {...props} />
      </React.Suspense>
    </Container>
  )
}

export default SuspenseWrapper
