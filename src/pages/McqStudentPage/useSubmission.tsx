import * as React from "react"

import { ISubmission, Mcq } from "../../services/api"

interface IOutput {
  submission: ISubmission | null
  loading: boolean
}

function useSubmission(
  mcqId: string,
  studentId: string,
  submitted: boolean,
): IOutput {
  const [loading, setLoading] = React.useState(true)
  const [submission, setSubmission] = React.useState<IOutput["submission"]>(
    null,
  )

  React.useEffect(
    () => {
      setLoading(true)
      Mcq.findSubmission(mcqId, studentId).then(data => {
        setSubmission(data)
        setLoading(false)
      })
    },
    [mcqId, studentId, submitted],
  )

  return { loading, submission }
}

export default useSubmission
