import styled from "styled-components"

import { appBarHeight, teacherColor } from "../../services/theme"

export default styled.div`
  width: 100vw;
  height: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  background-color: ${teacherColor};
`
