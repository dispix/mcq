import * as React from "react"
import { unstable_createResource } from "react-cache"
import { RouteComponentProps } from "react-router"
import { Link } from "react-router-dom"

import List from "../../components/List/List"
import { getGrade, Mcq } from "../../services/api"

import Container from "./Container"
import StudentResult from "./StudentResult"
import useSubmissions from "./useSubmissions"

export type Props = RouteComponentProps<{ id: string }>

const mcqResource = unstable_createResource(Mcq.getById)

function McqTeacherPage(props: Props) {
  const submissions = useSubmissions(props.match.params.id)
  const mcq = mcqResource.read(props.match.params.id)

  return (
    <Container>
      {submissions &&
        mcq &&
        (submissions.length ? (
          <List keys={submissions.map((_, i) => i)}>
            {submissions.map(submission => (
              <React.Suspense key={submission.student} fallback={null}>
                <StudentResult
                  studentId={submission.student}
                  result={[getGrade(mcq, submission), mcq.questions.length]}
                />
              </React.Suspense>
            ))}
            <Link to="/">Back</Link>
          </List>
        ) : (
          <p>No submission yet for this MCQ.</p>
        ))}
    </Container>
  )
}

export default McqTeacherPage
