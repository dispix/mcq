import * as React from "react"
import { unstable_createResource } from "react-cache"

import { User } from "../../services/api"

interface IProps {
  studentId: string
  result: [number, number]
}

const userResource = unstable_createResource(User.getById)

function StudentResult({ studentId, result }: IProps) {
  const user = userResource.read(studentId)
  const [score, max] = result

  return user ? (
    <p>
      {user.name} ({score}/{max})
    </p>
  ) : null
}

export default React.memo(StudentResult, () => true)
