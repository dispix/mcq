import * as React from "react"

import Container from "./Container"
import McqTeacherPage, { Props } from "./McqTeacherPage"

function SuspenseWrapper(props: Props) {
  return (
    <Container>
      <React.Suspense fallback={<p>Loading...</p>}>
        <McqTeacherPage {...props} />
      </React.Suspense>
    </Container>
  )
}

export default SuspenseWrapper
