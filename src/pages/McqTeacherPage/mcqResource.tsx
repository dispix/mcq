import { unstable_createResource } from "react-cache"

import { IMcq, Mcq } from "../../services/api"

export default unstable_createResource<string, IMcq | null>(Mcq.getById)
