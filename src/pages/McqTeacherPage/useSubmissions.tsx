import * as React from "react"

import { ISubmission, Mcq } from "../../services/api"

function useSubmissions(mcqId: string) {
  const [submissions, setSubmissions] = React.useState<ISubmission[] | null>(
    null,
  )

  React.useEffect(() => Mcq.subscribeToSubmissions(setSubmissions, mcqId), [
    mcqId,
  ])

  return submissions
}

export default useSubmissions
