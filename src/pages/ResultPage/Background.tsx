import styled from "styled-components"

import { appBarHeight, studentColor } from "../../services/theme"

const Background = styled.main`
  width: 100vw;
  height: 100%;
  display: flex;
  align-items: center;
  color: ${studentColor};
`

export default Background
