import firebase from "firebase"

import db from "./init"

export interface IQuestion {
  title: string
  availableAnswers: string[]
  validAnswers: number[]
}
export interface ISubmission {
  id: string
  student: string
  answers: Array<{
    question: number
    selection: number[]
  }>
}
export interface IMcq {
  id: string
  /**
   * ID of the teacher owning the Mcq
   */
  teacher: string
  title: string
  questions: IQuestion[]
  backgroundUri: string | null
}

export default class Mcq {
  /**
   * Gets the mcq data given its id
   * @param id the id of the mcq
   * @return the mcq object
   */
  public static async getById(id: string): Promise<null | IMcq> {
    const doc = await db
      .collection("mcqs")
      .doc(id)
      .get()

    return doc.exists ? Mcq.parseMcqDocument(doc) : null
  }

  /**
   * Gets all the available mcqs
   * @return all the mcqs
   */
  public static async getAll(): Promise<IMcq[]> {
    const snap = await db.collection("mcqs").get()

    return Promise.all(snap.docs.map(Mcq.parseMcqDocument))
  }

  /**
   * Submits the student answers to an mcq
   * @param mcqId identifier of the mcq
   * @param userId identifier of the student
   * @param answers answers submitted by the student
   */
  public static async submitAnswers(
    mcqId: string,
    userId: string,
    answers: number[][],
  ): Promise<string> {
    const submission = await db
      .collection("mcqs")
      .doc(mcqId)
      .collection("studentSubmissions")
      .add({
        answers: answers.map((selection, question) => ({
          question,
          selection,
        })),
        student: userId,
      })

    return submission.id
  }

  /**
   * Searches for a submission to the given mcq by the given student
   * @param mcqId id of the mcq
   * @param userId id of the student
   */
  public static async findSubmission(
    mcqId: string,
    userId: string,
  ): Promise<null | ISubmission> {
    const query = await db
      .collection("mcqs")
      .doc(mcqId)
      .collection("studentSubmissions")
      .where("student", "==", userId)
      .limit(1)
      .get()

    return query.size ? Mcq.parseSubmissionDocument(query.docs[0]) : null
  }

  /**
   * Deletes a submission given its id
   * @param mcqId id of the submission's mcq
   * @param submissionId id of the submission to delete
   */
  public static deleteSubmission(
    mcqId: string,
    submissionId: string,
  ): Promise<void> {
    return db
      .collection("mcqs")
      .doc(mcqId)
      .collection("studentSubmissions")
      .doc(submissionId)
      .delete()
  }

  /**
   * Calls the subscriber with the subsmissions for a given mcq
   * @param subscribeFn returns the list of submissions for the given mcq
   * @param mcqId id of the mcq
   */
  public static subscribeToSubmissions(
    subscribeFn: (data: ISubmission[]) => void,
    mcqId: string,
  ): () => void {
    return db
      .collection("mcqs")
      .doc(mcqId)
      .collection("studentSubmissions")
      .onSnapshot({ includeMetadataChanges: true }, querySnapshot => {
        subscribeFn(querySnapshot.docs.map(Mcq.parseSubmissionDocument))
      })
  }

  private static async parseMcqDocument(
    document: firebase.firestore.DocumentData,
  ): Promise<IMcq> {
    const id = document.id
    const data = document.data()
    const teacher = data.teacher.id
    const backgroundUri = data.backgroundUri
      ? await (firebase
          .storage()
          .refFromURL(data.backgroundUri)
          .getDownloadURL() as Promise<string>)
      : null

    return { ...data, id, teacher, backgroundUri }
  }

  private static parseSubmissionDocument(
    document: firebase.firestore.DocumentData,
  ): ISubmission {
    const id = document.id
    const data = document.data()
    const student = data.student

    return { ...data, id, student }
  }
}
