import db from "./init"

export interface IUser {
  name: string
  role: "TEACHER" | "STUDENT"
}

export default class User {
  /**
   * Finds a user given its infos
   * @param user the user name and role
   * @return the user id, if found
   */
  public static async findId(user: IUser): Promise<null | string> {
    const querySnapshot = await db
      .collection("users")
      .where("name", "==", user.name)
      .where("role", "==", user.role)
      .get()

    return querySnapshot.size ? querySnapshot.docs[0].id : null
  }

  /**
   * Gets the user data given its id
   * @param id the id of the user
   * @return the user object
   */
  public static async getById(id: string): Promise<null | IUser> {
    const doc = await db
      .collection("users")
      .doc(id)
      .get()

    return doc.exists ? (doc.data() as IUser) : null
  }

  /**
   * Creates a new user and returned the associated id
   * @param user the user informations
   * @return the newly created id
   */
  public static async create(user: IUser): Promise<string> {
    const document = await db.collection("users").add(user)

    return document.id
  }
}
