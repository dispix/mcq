import { IMcq, ISubmission } from "./Mcq"

export default function getGrade(mcq: IMcq, submission: ISubmission) {
  return mcq.questions
    .map((question, index) => {
      const studentAnswers = submission.answers.find(
        answer => answer.question === index,
      )

      if (studentAnswers) {
        return question.validAnswers.every(answer =>
          studentAnswers.selection.includes(answer),
        )
          ? true
          : false
      }

      return false
    })
    .filter(Boolean).length
}
