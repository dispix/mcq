import firebase from "firebase"

import { invariant } from "../../utils"

const baseConfig = {
  authDomain: "live-classroom.firebaseapp.com",
  databaseURL: "https://live-classroom.firebaseio.com",
  projectId: "live-classroom",
  storageBucket: "live-classroom.appspot.com",
}

const firestoreConfig: firebase.firestore.Settings = {
  timestampsInSnapshots: true,
}

function init() {
  const apiKey = process.env.REACT_APP_FIREBASE_API_KEY
  const messagingSenderId = process.env.REACT_APP_FIREBASE_MESSAGING_SENDER_ID

  invariant(!!apiKey, "Missing firebase api key.")
  invariant(!!messagingSenderId, "Missing firebase messagingSenderId.")

  const db = firebase
    .initializeApp({
      ...baseConfig,
      apiKey,
      messagingSenderId,
    })
    .firestore()

  db.settings(firestoreConfig)

  // @ts-ignore
  window.firebase = firebase
  return db
}

export default init()
