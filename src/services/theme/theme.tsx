import { ITheme } from "./types"

const theme: ITheme = {
  appBar: {
    height: "36px",
  },
  colors: {
    STUDENT: "#23BFE0",
    TEACHER: "#E08484",
  },
}

export default theme
