export interface IColors {
  STUDENT: string
  TEACHER: string
}

export interface ITheme {
  appBar: {
    height: string
  }
  colors: IColors
}
