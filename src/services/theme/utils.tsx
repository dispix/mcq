import compose from "ramda/es/compose"
import prop from "ramda/es/prop"

import { ITheme } from "./types"

export function themeProp<T extends keyof ITheme>(
  key: T,
): (props: { theme: ITheme }) => ITheme[T] {
  return compose(
    theme => theme[key],
    prop("theme"),
  )
}

export const studentColor = compose(
  colors => colors.STUDENT,
  themeProp("colors"),
)
export const teacherColor = compose(
  colors => colors.TEACHER,
  themeProp("colors"),
)
export const appBarHeight = compose(
  appBar => appBar.height,
  themeProp("appBar"),
)
