export default function invariant(test: boolean, message: string) {
  if (process.env.NODE_ENV !== "production" && !test) {
    throw new Error(message)
  }
}
